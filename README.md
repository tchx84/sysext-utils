# sysext-utils

sysext-utils is a collection of tools, built on top of [systemd-sysext](https://www.freedesktop.org/software/systemd/man/latest/systemd-sysext.html), that aims to provide a better experience developing and testing system components for [GNOME OS](https://os.gnome.org).

The basic idea is to facilitate building and managing system extensions that can be overlaid on top of the OS. This way, one can add modified or new components to its system to test. Check out the [original proposal](https://discourse.gnome.org/t/towards-a-better-way-to-hack-and-test-your-system-components/21075) for the rationale behind this project.

To achieve this, sysext-utils provides the following tools:

* sysext-build: creates an extension out of a [Meson](https://mesonbuild.com/) or [BuildStream](https://buildstream.build/) project, or an OS tree directory.
* sysext-add: imports, activates and integrates an extension.
* sysext-remove: deactivates and deintegrates an extension.
* sysext-install: combines both *sysext-build* and *sysext-add* functionalities into one, for convenience.
* sysext-build-element: creates an extension out of one or more BuildStream element, for convenience.

Although this project is primarily designed for GNOME OS, the tool itself and its principles could be expanded to any other OS with systemd version 256 or newer.

## Requirements

* An installation of [GNOME OS](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/wikis/gnome_os/Start-GNOME-OS#getting-the-images) with the sysupdate variant.
* The development tree [sysext](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/wikis/gnome_os/Install-Software#install-developer-tools-and-system-extensions-with-systemd-sysext) of GNOME OS, to pull all required dependencies.

Alternatively, the core GNOME OS OCI image can be used to build extensions with *sysext-build*. A few extra dependencies might be needed, depending on the target build system and extension image format. See this [example](./docker).

## Installation

To install sysext-utils simply follow these steps:

```bash
$ git clone https://gitlab.gnome.org/tchx84/sysext-utils.git
$ cd sysext-utils
$ python3 -m venv env
$ source env/bin/activate
$ python -m pip install .
```

## Usage

### Building extensions

*sysext-build* can facilitate the creation of extensions out of projects with different build systems.

#### Meson

```bash
$ git clone https://gitlab.gnome.org/GNOME/gtk.git
$ cd gtk
$ meson setup ./build --prefix /usr --libdir="lib/$(gcc -print-multiarch)"
# do development
$ sysext-build example ./build
# obtain example.sysext.raw or example.confext.raw extension images
```

#### BuildStream

```bash
$ git clone https://gitlab.gnome.org/GNOME/gnome-build-meta
$ cd gnome-build-meta
$ bst workspace open --directory ./workspace sdk/gtk.bst
# do development
$ sysext-build example ./workspace
# obtain example.sysext.raw or example.confext.raw extension images
```

#### An OS tree directory

```bash
$ git clone https://gitlab.gnome.org/tchx84/sysext-utils.git
$ cd sysext-utils
# do development and use another build system to produce an OS tree directory
$ sysext-build example ./tests/data/example
# obtain example.sysext.raw or example.confext.raw extension images
```

#### Notes

* The first argument to *sysext-build* is the name to be given to the extension. The name can be any valid filename.
* The second argument must be a directory. *sysext-build* will try to detect the proper build system to use. If needed, one can force a build system by using the `--system` optional argument.
* By default, *sysext-build* will use *mksquashfs* to make the extension images. Images can also be signed by specifying the `--format=ddi` optional argument, along with the `--private-key` and `--certificate` arguments.
* By default, *sysext-build* will set metadata to ensure that the extension can only be used in identical systems. This behavior can be overridden by using the `--ignore-release` optional argument. This is useful when building extensions in a separate environment like in a CI pipeline.
* By default, *sysext-build* will detect the available OS tree directories and build images with the proper extension type. If needed, one can specify a target extension type by using the `--type` optional argument e.g., to only build the sysext image.
* By default, *sysext-build* will create images only for extension types it detects. One can force the creation of images for all extension types by using the `--force` optional argument. Although this can result in empty extension images, it can facilitate the creation of CI pipelines that require both images.
* See `sysext-build --help` for more details.

### Managing extensions

Whether the extension is available locally or remotely, it can be added to the system as follows:

#### Adding an extension

```bash
$ sysext-add example.confext.raw example.sysext.raw
$ sysext-add https://os.gnome.org/sysext/example.sysext.raw
# do testing
```

#### Removing an extension

```bash
$ sysext-remove example
```

#### Notes

* Adding an extension will run integration steps like recompiling the glib schemas and updating the icons cache. These integration artifacts are added in a separate extension which is global for all extensions.
* Removing an extension will also run these integration steps. The global integration extension will be removed along with the last extension.
* By default, extensions are added under `/run/extensions`. This means that the extension will be removed after a system reboot, which is a safer default behavior. This can be overridden by using the `--persistent` optional argument.
* By default, *sysext-add* will accept one or more *sysext* and *confext* images. If needed, one can specify a target extension type by using the `--type` optional argument e.g., to only add the sysext images.
* By default, *sysext-remove* will lookup for both *sysext* and *confext* extensions with the given name and remove both. If needed, one can specify a target extension type by using the `--type` optional argument e.g., to only remove the sysext extension.
* See `sysext-add --help` and `sysext-remove --help` for more details.

### Facilitating local development

Constantly building and adding extensions can be tedious when testing things locally, therefore a *sysext-install* convenience tool is included to do both in a single step.

#### A single step

```bash
$ git clone https://gitlab.gnome.org/GNOME/gtk.git
$ cd gtk
$ meson setup ./build --prefix /usr --libdir="lib/$(gcc -print-multiarch)"
# do development
$ sysext-install example ./build
# do testing
```

#### Notes

* *sysext-install* combines both *sysext-build* and *sysext-add* functionalities and arguments and therefore follows the exact same defaults.
* See `sysext-install --help` for more details.

### Facilitating CI pipelines

A common workflow is to build extensions directly from one or more BuildStream element, therefore a *sysext-build-element* convenience tool is included.

#### Building from elements

```bash
$ git clone https://gitlab.gnome.org/GNOME/gnome-build-meta.git
$ cd gnome-build-meta
$ sysext-build-element example sdk/inter-font.bst sdk/gtk.bst
# obtain the example.sysext.raw extension image
```

#### Notes

* *sysext-build-element* arguments are similar to *sysext-build* arguments, therefore follow the same defaults.
* See `sysext-build-element --help` for more details.

## What else is missing?

See the list of [issues](https://gitlab.gnome.org/tchx84/sysext-utils/-/issues) for opportunities to improve this tool.

## License

MIT License

Copyright (c) 2024 Codethink Limited

Copyright (c) 2024 GNOME Foundation

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
