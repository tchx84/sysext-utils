# Changelog

## [0.3.0] - 2024-10-29

* Added support for building sysext system extensions out of the /opt/ tree.
* Added support for detecting, building and managing confext system extensions.
* Added integration step for gio-querymodules.
* Added sysext-build-element tool to build one or more BuildStream elements.
* Fixed sysext-remove tool not cleaning up its workspace.
* Fixed naming consistency of CLI arguments by renaming the private key argument to `--private-key`.

## [0.2.1] - 2024-08-09

* Fixed preserving symbolic links when copying directories.
* Fixed examples in the README.
* Added CI to publish system extensions with releases.

## [0.2.0] - 2024-06-11

* Initial public release
