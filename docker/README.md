# sysext-utils Toolbx

This directory contains a sample Dockerfile based on the [core](https://gitlab.gnome.org/GNOME/gnome-build-meta) GNOME OS OCI image, plus *mksquashfs* and *sysext-utils* itself. This can create a suitable Toolbx environment to create images that are not signed, out of any GNOME project with the meson build system.

## Build it

```bash
$ podman build . -t sysext-utils:latest
$ toolbox create -c sysext-utils -i sysext-utils
```

## Use it

```bash
$ toolbox enter sysext-utils
```
