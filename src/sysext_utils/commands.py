# MIT License
#
# Copyright (c) 2024 Codethink Limited
# Copyright (c) 2024 GNOME Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# SPDX-License-Identifier: MIT

import subprocess

from typing import List

from . import config

from .errors import CommandNotAvailableError, CommandFailedError
from .definitions import DryStepType, ExtensionType


class BaseCommand:
    @classmethod
    def run(cls, *args) -> None:
        raise NotImplementedError()

    @classmethod
    def do_run(cls, command: str, *args) -> None:
        commands = [command] + list(*args)

        if config.dry_run:
            print([str(DryStepType.RUNNING)] + commands)
            return

        try:
            subprocess.run(
                commands,
                stdout=None if config.verbose else subprocess.PIPE,
                stderr=subprocess.STDOUT,
                check=True,
            )
        except FileNotFoundError:
            raise CommandNotAvailableError(command)
        except subprocess.CalledProcessError as e:
            raise CommandFailedError(command, e.stdout)


class GLibCompileSchemas(BaseCommand):
    @classmethod
    def compile(cls, source: str, destination: str) -> None:
        cls.run(
            [
                "--strict",
                source,
                "--targetdir",
                destination,
            ]
        )

    @classmethod
    def run(cls, *args) -> None:
        cls.do_run("glib-compile-schemas", *args)


class Gtk4UpdateIconCache(BaseCommand):
    @classmethod
    def update(cls, path: str) -> None:
        cls.run(
            [
                "--quiet",
                "--ignore-theme-index",
                "--force",
                path,
            ]
        )

    @classmethod
    def run(cls, *args) -> None:
        cls.do_run("gtk4-update-icon-cache", *args)


class GioQueryModules(BaseCommand):
    @classmethod
    def run(cls, *args) -> None:
        cls.do_run("gio-querymodules", list(args))


class Importctl(BaseCommand):
    @classmethod
    def import_raw(cls, name: str, image: str, extension_type: str) -> None:
        cls.run(
            [
                "import-raw",
                f"--class={extension_type}",
                image,
                name,
            ]
        )

    @classmethod
    def pull_raw(cls, name: str, url: str, extension_type: str) -> None:
        cls.run(
            [
                "pull-raw",
                "--verify=no",
                f"--class={extension_type}",
                url,
                name,
            ]
        )

    @classmethod
    def run(cls, *args) -> None:
        cls.do_run("importctl", *args)


class Mksquashfs(BaseCommand):
    @classmethod
    def build(
        cls,
        source: str,
        path: str,
        extension_type: str,
    ) -> None:
        include = ["usr", "opt"] if extension_type == ExtensionType.SYSEXT else ["etc"]
        cls.run(
            [
                source,
                path,
                "-wildcards",
                "-e",
                f"!({'|'.join(include)})",
            ]
        )

    @classmethod
    def run(cls, *args) -> None:
        cls.do_run("mksquashfs", *args)


class SystemdRepart(BaseCommand):
    @classmethod
    def build(
        cls,
        source: str,
        path: str,
        private_key: str,
        certificate: str,
        extension_type: str,
    ) -> None:
        cls.run(
            [
                f"--make-ddi={extension_type}",
                "--offline=true",
                "--seed=random",
                f"--private-key={private_key}",
                f"--certificate={certificate}",
                f"--copy-source={source}",
                path,
            ]
        )

    @classmethod
    def run(cls, *args) -> None:
        cls.do_run("systemd-repart", *args)


class SystemdSysext(BaseCommand):
    @classmethod
    def refresh(cls) -> None:
        cls.run(["refresh"])

    @classmethod
    def run(cls, *args) -> None:
        cls.do_run("systemd-sysext", *args)


class SystemdConfext(BaseCommand):
    @classmethod
    def refresh(cls) -> None:
        cls.run(["refresh"])

    @classmethod
    def run(cls, *args) -> None:
        cls.do_run("systemd-confext", *args)


class Bst(BaseCommand):
    @classmethod
    def build_workspace(cls, workspace: str) -> None:
        cls.run(
            [
                "--directory",
                workspace,
                "build",
                "--retry-failed",
            ]
        )

    @classmethod
    def build_element(cls, element: List[str]) -> None:
        cls.run(
            [
                "build",
                *element,
                "--retry-failed",
            ]
        )

    @classmethod
    def artifact_checkout_workspace(cls, workspace: str, directory: str) -> None:
        cls.run(
            [
                "--directory",
                workspace,
                "artifact",
                "checkout",
                "--force",
                "--deps=none",
                "--directory",
                directory,
            ]
        )

    @classmethod
    def artifact_checkout_element(cls, element: str, directory: str) -> None:
        cls.run(
            [
                "artifact",
                "checkout",
                element,
                "--force",
                "--deps=none",
                "--directory",
                directory,
            ]
        )

    @classmethod
    def run(cls, *args) -> None:
        cls.do_run("bst", *args)


class Meson(BaseCommand):
    @classmethod
    def compile(cls, build_directory: str) -> None:
        cls.run(["compile", "-C", build_directory])

    @classmethod
    def install(cls, build_directory: str, destination: str) -> None:
        cls.run(["install", "-C", build_directory, "--destdir", destination])

    @classmethod
    def run(cls, *args) -> None:
        cls.do_run("meson", *args)
