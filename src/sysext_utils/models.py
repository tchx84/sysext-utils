# MIT License
#
# Copyright (c) 2024 Codethink Limited
# Copyright (c) 2024 GNOME Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# SPDX-License-Identifier: MIT

import os

from typing import Dict, Optional

from .errors import (
    IncompatibleSettingError,
    IncompatibleExtensionTypeError,
    UnsupportedImageError,
)
from .helpers import is_url, get_url_filename, check_path
from .definitions import (
    ExtensionType,
    SYSEXT_IMAGE_SUFFIX,
    METADATA_DIR,
    VAR_EXTENSIONS_DIR,
    RUN_EXTENSIONS_DIR,
    CONFEXT_IMAGE_SUFFIX,
    METADATA_CONFEXT_DIR,
    VAR_CONFEXTS_DIR,
    RUN_CONFEXTS_DIR,
    INTEGRATION_SYSEXT_NAME,
)


class Setting:

    __suffix__: Dict[str, str] = {
        ExtensionType.SYSEXT: SYSEXT_IMAGE_SUFFIX,
        ExtensionType.CONFEXT: CONFEXT_IMAGE_SUFFIX,
    }

    __metadata__: Dict[str, str] = {
        ExtensionType.SYSEXT: METADATA_DIR,
        ExtensionType.CONFEXT: METADATA_CONFEXT_DIR,
    }

    __installation__: Dict[ExtensionType, Dict[bool, str]] = {
        ExtensionType.SYSEXT: {
            True: VAR_EXTENSIONS_DIR,
            False: RUN_EXTENSIONS_DIR,
        },
        ExtensionType.CONFEXT: {
            True: VAR_CONFEXTS_DIR,
            False: RUN_CONFEXTS_DIR,
        },
    }

    def __init__(
        self,
        extension_type: ExtensionType = ExtensionType.SYSEXT,
        persistent: bool = False,
    ) -> None:
        self.extension_type = extension_type
        self.persistent = persistent

    @property
    def suffix(self) -> str:
        return self.__suffix__[self.extension_type]

    @property
    def metadata_dir(self) -> str:
        return self.__metadata__[self.extension_type]

    @property
    def installation_dir(self) -> str:
        return self.__installation__[self.extension_type][self.persistent]

    @property
    def integration_path(self) -> str:
        directory = self.__installation__[ExtensionType.SYSEXT][self.persistent]
        return os.path.join(directory, INTEGRATION_SYSEXT_NAME)

    def get_installed_dir(self, name: str) -> str:
        return os.path.join(self.installation_dir, name)

    def get_installed_path(self, name: str) -> str:
        return f"{self.get_installed_dir(name)}.raw"

    def get_installed_path_default(self, name: str) -> str:
        default = self.__installation__[self.extension_type][True]
        return f"{os.path.join(default, name)}.raw"

    def get_destination_path(self, name: str, directory: str) -> str:
        return os.path.join(directory, f"{name}{self.suffix}")

    def get_image_name(self, image: str) -> str:
        image = get_url_filename(image) if is_url(image) else image
        return os.path.basename(image).removesuffix(self.suffix)

    def ensure(self, image: str) -> "Setting":
        if not image.endswith(self.__suffix__[self.extension_type]):
            raise IncompatibleExtensionTypeError(self.extension_type)

        if self.extension_type != ExtensionType.SYSEXT:
            return self

        if self.persistent is False and os.path.exists(
            os.path.join(VAR_EXTENSIONS_DIR, INTEGRATION_SYSEXT_NAME)
        ):
            raise IncompatibleSettingError()
        if self.persistent is True and os.path.exists(
            os.path.join(RUN_EXTENSIONS_DIR, INTEGRATION_SYSEXT_NAME)
        ):
            raise IncompatibleSettingError()

        return self

    @classmethod
    def find(cls, name: str, extension_type: ExtensionType) -> Optional["Setting"]:
        for persistent in [True, False]:
            path = cls.__installation__[extension_type][persistent]
            for filename in [name, f"{name}.raw"]:
                if check_path(os.path.join(path, filename)):
                    return cls(extension_type, persistent)

        return None

    @classmethod
    def guess(cls, image: str, persistent: bool) -> "Setting":
        if image.endswith(SYSEXT_IMAGE_SUFFIX):
            return cls(ExtensionType.SYSEXT, persistent).ensure(image)
        elif image.endswith(CONFEXT_IMAGE_SUFFIX):
            return cls(ExtensionType.CONFEXT, persistent).ensure(image)

        raise UnsupportedImageError(image)
