# MIT License
#
# Copyright (c) 2024 Codethink Limited
# Copyright (c) 2024 GNOME Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# SPDX-License-Identifier: MIT

from typing import Optional


class HandledError(BaseException):
    pass


class NotPriviledgedError(HandledError):
    def __init__(self):
        super().__init__("Need to be privileged.")


class CommandNotAvailableError(HandledError):
    def __init__(self, command: str):
        super().__init__(f"{command} is not installed.")


class CommandFailedError(HandledError):
    def __init__(self, command: str, output: Optional[bytes]):
        if output is not None:
            message = f"{command} failed due to: {output.decode('UTF-8')}"
        else:
            message = f"{command} failed."

        super().__init__(message)


class EmptyDirectoryError(HandledError):
    def __init__(self, directory: str):
        super().__init__(f"No contents were found at: {directory}")


class UnsupportedImageFormatError(HandledError):
    def __init__(self, image_format: str):
        super().__init__(f"Image format not supported: {image_format}")


class UnsupportedBuildSystemError(HandledError):
    def __init__(self, system: str):
        super().__init__(f"Build system not supported: {system}")


class UnsupportedImageError(HandledError):
    def __init__(self, image: str):
        super().__init__(f"Unsupported image: {image}")


class IncompatibleExtensionTypeError(HandledError):
    def __init__(self, extension_type: str):
        super().__init__(f"Extension type is not: {extension_type}")


class IncompatibleSettingError(HandledError):
    def __init__(self):
        super().__init__("Extensions must be either all persistent or runtime")
