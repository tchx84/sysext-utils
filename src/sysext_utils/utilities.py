# MIT License
#
# Copyright (c) 2024 Codethink Limited
# Copyright (c) 2024 GNOME Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# SPDX-License-Identifier: MIT

import os

from typing import List, Tuple

from .errors import UnsupportedImageFormatError, UnsupportedBuildSystemError
from .definitions import (
    ImageFormat,
    BuildSystemType,
    ExtensionType,
    SCHEMAS_DIR,
    ICONS_DIR,
    ICONS_CACHE_FILENAME,
    QUERY_MODULES_DIR,
    QUERY_MODULES_CACHE_FILENAME,
    METADATA_PREFIX,
)
from .commands import (
    SystemdRepart,
    Mksquashfs,
    Importctl,
    SystemdSysext,
    SystemdConfext,
    GLibCompileSchemas,
    Gtk4UpdateIconCache,
    GioQueryModules,
    Bst,
    Meson,
)
from .helpers import (
    get_sysext_metadata,
    get_sysext_tree,
    get_sysext_tree_opt,
    get_confext_tree,
    is_url,
    get_path_for_artifact,
    get_path_for_integration_artifact,
    guess_system_from_source,
    make_directory,
    copy_directory,
    remove_directory,
    ensure_directory,
    write_file,
    move_file,
    remove_file,
    check_path,
)
from .models import Setting


def refresh(setting: Setting) -> None:
    if setting.extension_type == ExtensionType.SYSEXT:
        SystemdSysext.refresh()
    elif setting.extension_type == ExtensionType.CONFEXT:
        SystemdConfext.refresh()


def build_image(
    name: str,
    artifact: str,
    destination: str,
    image_format: str,
    private_key: str,
    certificate: str,
    ignore_release: bool,
    setting: Setting,
) -> str:
    image = setting.get_destination_path(name, destination)

    remove_file(image)
    make_directory(destination)

    build_tree(name, artifact, ignore_release, setting)

    if image_format == ImageFormat.DDI:
        SystemdRepart.build(
            artifact, image, private_key, certificate, setting.extension_type
        )
    elif image_format == ImageFormat.COMPRESSED:
        Mksquashfs.build(artifact, image, setting.extension_type)
    else:
        raise UnsupportedImageFormatError(image_format)

    return image


def build_tree(
    name: str,
    artifact: str,
    ignore_release: bool,
    setting: Setting,
) -> None:
    metadata_directory = os.path.join(artifact, setting.metadata_dir)
    metadata_path = os.path.join(metadata_directory, f"{METADATA_PREFIX}{name}")
    metadata_content = get_sysext_metadata(ignore_release)

    make_directory(metadata_directory)
    write_file(metadata_content, metadata_path)


def add_image(name: str, image: str, setting: Setting) -> None:
    if is_url(image):
        Importctl.pull_raw(name, image, setting.extension_type)
    else:
        Importctl.import_raw(name, image, setting.extension_type)

    # See https://github.com/systemd/systemd/issues/32938
    if not setting.persistent:
        make_directory(setting.installation_dir)
        move_file(
            setting.get_installed_path_default(name),
            setting.get_installed_path(name),
        )

    refresh(setting)


def add_tree(name: str, artifact: str, setting: Setting) -> None:
    copy_directory(artifact, setting.get_installed_dir(name))

    refresh(setting)


def remove_image(name: str, setting: Setting) -> None:
    remove_file(setting.get_installed_path(name))

    refresh(setting)


def remove_tree(name: str, setting: Setting) -> None:
    remove_directory(setting.get_installed_dir(name))

    refresh(setting)


def build_integration_artifact() -> str:
    artifact = get_path_for_integration_artifact()

    remove_directory(artifact)
    make_directory(artifact)

    build_integration_schemas(artifact)
    build_integration_icons(artifact)
    build_integration_query_modules(artifact)

    ensure_directory(artifact)

    return artifact


def build_integration_schemas(artifact: str) -> None:
    host_schemas_dir = os.path.join(os.sep, SCHEMAS_DIR)
    artifact_schemas_dir = os.path.join(artifact, SCHEMAS_DIR)

    make_directory(artifact_schemas_dir)
    GLibCompileSchemas.compile(host_schemas_dir, artifact_schemas_dir)


def build_integration_icons(artifact: str) -> None:
    host_icons_dir = os.path.join(os.sep, ICONS_DIR)
    tmp_icons_dir = os.path.join(artifact, ICONS_DIR + ".tmp")
    artifact_icons_dir = os.path.join(artifact, ICONS_DIR)

    make_directory(tmp_icons_dir)
    copy_directory(host_icons_dir, tmp_icons_dir)
    Gtk4UpdateIconCache.update(tmp_icons_dir)

    make_directory(artifact_icons_dir)
    move_file(
        os.path.join(tmp_icons_dir, ICONS_CACHE_FILENAME),
        os.path.join(artifact_icons_dir, ICONS_CACHE_FILENAME),
    )
    remove_directory(tmp_icons_dir)


def build_integration_query_modules(artifact: str) -> None:
    host_modules_dir = os.path.join(os.sep, QUERY_MODULES_DIR)
    tmp_modules_dir = os.path.join(artifact, QUERY_MODULES_DIR + ".tmp")
    artifact_modules_dir = os.path.join(artifact, QUERY_MODULES_DIR)

    make_directory(tmp_modules_dir)
    copy_directory(host_modules_dir, tmp_modules_dir)
    GioQueryModules.run(tmp_modules_dir)

    if not check_path(os.path.join(tmp_modules_dir, QUERY_MODULES_CACHE_FILENAME)):
        return

    make_directory(artifact_modules_dir)
    move_file(
        os.path.join(tmp_modules_dir, QUERY_MODULES_CACHE_FILENAME),
        os.path.join(artifact_modules_dir, QUERY_MODULES_CACHE_FILENAME),
    )
    remove_directory(tmp_modules_dir)


def build_artifact(
    source: str,
    element: List[str],
    system: str,
) -> Tuple[str, List[ExtensionType]]:
    destination = get_path_for_artifact()
    extension_types = {}

    remove_directory(destination)
    make_directory(destination)

    if system == BuildSystemType.AUTO:
        system = guess_system_from_source(source)
    if system == BuildSystemType.BST:
        build_artifact_bst_workspace(source, destination)
    elif system == BuildSystemType.BST_ELEMENT:
        build_artifact_bst_element(element, destination)
    elif system == BuildSystemType.MESON:
        build_artifact_meson_project(source, destination)
    elif system == BuildSystemType.IMPORT:
        build_artifact_import_directory(source, destination)
    else:
        raise UnsupportedBuildSystemError(system)

    ensure_directory(destination)

    # preserve order so confext is always processed first
    if check_path(get_confext_tree(destination)):
        extension_types[ExtensionType.CONFEXT] = True
    if check_path(get_sysext_tree(destination)):
        extension_types[ExtensionType.SYSEXT] = True
    if check_path(get_sysext_tree_opt(destination)):
        extension_types[ExtensionType.SYSEXT] = True

    return destination, list(extension_types)


def build_artifact_bst_workspace(workspace: str, directory: str) -> None:
    Bst.build_workspace(workspace)
    Bst.artifact_checkout_workspace(workspace, directory)


def build_artifact_bst_element(element: List[str], directory: str) -> None:
    Bst.build_element(element)
    for sub_element in element:
        Bst.artifact_checkout_element(sub_element, directory)


def build_artifact_meson_project(build_directory: str, destination: str) -> None:
    Meson.compile(build_directory)
    Meson.install(build_directory, destination)


def build_artifact_import_directory(source: str, destination: str) -> None:
    ensure_directory(source)
    copy_directory(source, destination)
