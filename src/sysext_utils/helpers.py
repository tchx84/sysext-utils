# MIT License
#
# Copyright (c) 2024 Codethink Limited
# Copyright (c) 2024 GNOME Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# SPDX-License-Identifier: MIT

import os
import re
import shutil

from urllib.parse import urlparse
from typing import Any, Dict, List, Callable

from . import config

from .version import version
from .errors import (
    NotPriviledgedError,
    EmptyDirectoryError,
)
from .definitions import (
    METADATA_DIR,
    METADATA_PREFIX,
    WORKSPACE_DIR,
    OS_RELEASE_PATH,
    METADATA_CUSTOM_FIELD,
    BuildSystemType,
    DryStepType,
)


def get_release_data(path: str) -> Dict[str, str]:
    data = {}

    try:
        with open(path, encoding="utf-8") as f:
            data = dict([l.strip().split("=", 1) for l in f.readlines()])
    except FileNotFoundError:
        pass

    return data


def get_sysext_metadata(ignore_release: bool) -> str:
    metadata = {
        f"{METADATA_CUSTOM_FIELD}": version,
    }

    if ignore_release:
        metadata["ID"] = "_any"
    else:
        release = get_release_data(OS_RELEASE_PATH)
        metadata["ID"] = release.get("ID", "_any")
        metadata["VERSION_ID"] = release.get("VERSION_ID", "_any")

    return "\n".join([f"{k}={v}" for k, v in metadata.items()])


def get_active_sysext_names(metadata_dir: str = METADATA_DIR) -> List[str]:
    names: List[str] = []

    path = os.path.join(os.sep, metadata_dir)

    if not os.path.exists(path):
        return names

    for filename in os.listdir(path):
        release = get_release_data(os.path.join(path, filename))

        if not release.get(METADATA_CUSTOM_FIELD):
            continue

        names.append(filename.removeprefix(METADATA_PREFIX))

    return names


def is_url(string: str) -> bool:
    return re.match("http[s]?://", string) is not None


def get_url_filename(url: str) -> str:
    return urlparse(url).path


def guess_system_from_source(source: str) -> str:
    if os.path.exists(os.path.join(source, "meson-info")):
        return BuildSystemType.MESON
    elif os.path.exists(os.path.join(source, ".bstproject.yaml")):
        return BuildSystemType.BST
    elif os.path.exists(get_sysext_tree(source)):
        return BuildSystemType.IMPORT
    elif os.path.exists(get_sysext_tree_opt(source)):
        return BuildSystemType.IMPORT
    elif os.path.exists(get_confext_tree(source)):
        return BuildSystemType.IMPORT

    return BuildSystemType.UNKOWN


def get_sysext_tree(root: str = "") -> str:
    return os.path.join(root, "usr")


def get_sysext_tree_opt(root: str = "") -> str:
    return os.path.join(root, "opt")


def get_confext_tree(root: str = "") -> str:
    return os.path.join(root, "etc")


def get_path_for_artifact() -> str:
    return os.path.join(WORKSPACE_DIR, "artifact")


def get_path_for_integration_artifact() -> str:
    return os.path.join(WORKSPACE_DIR, "integration")


def dryable(step: DryStepType, result: Any = None) -> Callable:
    def wrap(helper) -> Callable:
        def wrapped(*args) -> Any:
            if config.dry_run:
                print([str(step)] + list(args))
                return result
            else:
                return helper(*args)

        return wrapped

    return wrap


@dryable(step=DryStepType.MAKING)
def make_directory(directory: str) -> None:
    os.makedirs(directory, exist_ok=True)


@dryable(step=DryStepType.COPYING)
def copy_directory(source: str, destination: str) -> None:
    shutil.copytree(source, destination, dirs_exist_ok=True, symlinks=True)


@dryable(step=DryStepType.REMOVING)
def remove_directory(directory: str) -> None:
    shutil.rmtree(directory, ignore_errors=True)


@dryable(step=DryStepType.WRITING)
def write_file(content: str, path: str) -> None:
    with open(path, "w", encoding="utf-8") as f:
        f.write(content)


@dryable(step=DryStepType.MOVING)
def move_file(source: str, destination: str) -> None:
    shutil.move(source, destination)


@dryable(step=DryStepType.REMOVING)
def remove_file(path: str) -> None:
    try:
        os.remove(path)
    except FileNotFoundError:
        pass


@dryable(step=DryStepType.CHECKING, result=True)
def check_path(path: str) -> bool:
    return os.path.exists(path)


def ensure_priviledged() -> None:
    if not config.dry_run and os.getuid() != 0:
        raise NotPriviledgedError()


def ensure_directory(path: str) -> None:
    if not config.dry_run and (not os.path.isdir(path) or not os.listdir(path)):
        raise EmptyDirectoryError(path)
