# MIT License
#
# Copyright (c) 2024 Codethink Limited
# Copyright (c) 2024 GNOME Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# SPDX-License-Identifier: MIT

import os
import re
import sys
import argparse

from . import config
from .errors import HandledError
from .definitions import ImageFormat, BuildSystemType, ExtensionType, WORKSPACE_DIR
from .version import version
from .workflows import (
    run_image_build_workflow,
    run_image_add_workflow,
    run_image_remove_workflow,
    run_cleanup_workflow,
)


def _name_validator(name: str) -> str:
    if not re.match("^[\\w\\-.]+$", name):
        raise argparse.ArgumentTypeError(f"'{name}' is not a valid sysext name")
    return name


def _build_args_validator(
    parser: argparse.ArgumentParser,
    args: argparse.Namespace,
) -> None:
    if args.format == ImageFormat.DDI and not (args.private_key and args.certificate):
        parser.error("DDI image creation require private key and certificate")


def _build_base_parser(name: str, description: str) -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        prog=name,
        description=description,
    )
    parser.add_argument(
        "--version",
        help="print the current version of this utility",
        action="version",
        version=version,
    )
    parser.add_argument(
        "--dry",
        help="prints to stdout the commands involved in the operation",
        action="store_true",
    )
    parser.add_argument(
        "--verbose",
        help="captures all system calls stdout and stderr",
        action="store_true",
    )
    parser.add_argument(
        "--type",
        dest="extension_type",
        help="extension type to create, e.g., sysext",
        choices=[
            str(ExtensionType.AUTO),
            str(ExtensionType.SYSEXT),
            str(ExtensionType.CONFEXT),
        ],
        default=ExtensionType.AUTO,
    )

    return parser


def _handle_global_args(
    parser: argparse.ArgumentParser,
    args: argparse.Namespace,
) -> None:
    config.dry_run = args.dry
    config.verbose = args.verbose


def add():
    parser = _build_base_parser("sysext-add", "add an image to the system")
    parser.add_argument(
        "images",
        nargs="+",
        help="path or URL to the sysext image e.g., sdk.sysext.raw",
    )
    parser.add_argument(
        "--persistent",
        help="add the image under /var/lib/extensions/ to persist after reboots",
        action="store_true",
    )

    args = parser.parse_args()
    _handle_global_args(parser, args)

    try:
        run_image_add_workflow(args.images, args.persistent, args.extension_type)
    except HandledError as e:
        sys.exit(str(e))
    finally:
        run_cleanup_workflow()


def remove():
    parser = _build_base_parser("sysext-remove", "remove an image from the system")
    parser.add_argument(
        "name",
        help="name of the the sysext e.g., sdk",
        type=_name_validator,
    )

    args = parser.parse_args()
    _handle_global_args(parser, args)

    try:
        run_image_remove_workflow(args.name, args.extension_type)
    except HandledError as e:
        sys.exit(str(e))
    finally:
        run_cleanup_workflow()


def build():
    parser = _build_base_parser("sysext-build", "build an image from a given directory")
    parser.add_argument(
        "name",
        help="what to name the sysext e.g., sdk",
        type=_name_validator,
    )
    parser.add_argument(
        "source",
        help="directory to build the image from e.g., ./destdir/",
        default=os.path.abspath(os.getcwd()),
    )
    parser.add_argument(
        "--destination",
        help="directory to write the image to e.g., ./images/. Defaults to $PWD",
        default=os.path.abspath(os.getcwd()),
    )
    parser.add_argument(
        "--format",
        help="format to be used for the image creation",
        choices=[str(ImageFormat.DDI), str(ImageFormat.COMPRESSED)],
        default=ImageFormat.COMPRESSED,
    )
    parser.add_argument(
        "--private-key",
        help="path to the private key file e.g., files/boot-keys/SYSEXT.key",
        default="",
    )
    parser.add_argument(
        "--certificate",
        help="path to the certificate file, e.g., files/boot-keys/SYSEXT.crt",
        default="",
    )
    parser.add_argument(
        "--ignore-release",
        help="ignore the host release data and use 'ID=_any' instead",
        action="store_true",
    )
    parser.add_argument(
        "--system",
        help="build system to use e.g., import",
        choices=[
            str(BuildSystemType.AUTO),
            str(BuildSystemType.BST),
            str(BuildSystemType.MESON),
            str(BuildSystemType.IMPORT),
        ],
        default=BuildSystemType.AUTO,
    )
    parser.add_argument(
        "--force",
        help="force the creation of images for all the target extension types",
        action="store_true",
    )

    args = parser.parse_args()
    _handle_global_args(parser, args)
    _build_args_validator(parser, args)

    try:
        run_image_build_workflow(
            args.name,
            args.source,
            [],
            args.destination,
            args.format,
            args.private_key,
            args.certificate,
            args.ignore_release,
            args.system,
            args.extension_type,
            args.force,
        )
    except HandledError as e:
        sys.exit(str(e))
    finally:
        run_cleanup_workflow()


def build_element():
    parser = _build_base_parser(
        "sysext-build-element", "build an image from one or more bst element"
    )
    parser.add_argument(
        "name",
        help="what to name the sysext e.g., sdk",
        type=_name_validator,
    )
    parser.add_argument(
        "element",
        nargs="+",
        help="One or more bst element e.g., sdk/gtk.bst",
    )
    parser.add_argument(
        "--destination",
        help="directory to write the image to e.g., ./images/. Defaults to $PWD",
        default=os.path.abspath(os.getcwd()),
    )
    parser.add_argument(
        "--format",
        help="format to be used for the image creation",
        choices=[str(ImageFormat.DDI), str(ImageFormat.COMPRESSED)],
        default=ImageFormat.COMPRESSED,
    )
    parser.add_argument(
        "--private-key",
        help="path to the private key file e.g., files/boot-keys/SYSEXT.key",
        default="",
    )
    parser.add_argument(
        "--certificate",
        help="path to the certificate file, e.g., files/boot-keys/SYSEXT.crt",
        default="",
    )
    parser.add_argument(
        "--ignore-release",
        help="ignore the host release data and use 'ID=_any' instead",
        action="store_true",
    )
    parser.add_argument(
        "--force",
        help="force the creation of images for all the target extension types",
        action="store_true",
    )

    args = parser.parse_args()
    _handle_global_args(parser, args)
    _build_args_validator(parser, args)

    try:
        run_image_build_workflow(
            args.name,
            "",
            args.element,
            args.destination,
            args.format,
            args.private_key,
            args.certificate,
            args.ignore_release,
            BuildSystemType.BST_ELEMENT,
            args.extension_type,
            args.force,
        )
    except HandledError as e:
        sys.exit(str(e))
    finally:
        run_cleanup_workflow()


def install():
    parser = _build_base_parser("sysext-install", "build and add an image")
    parser.add_argument(
        "name",
        help="what to name the sysext e.g., sdk",
        type=_name_validator,
    )
    parser.add_argument(
        "source",
        help="directory to build the image from e.g., ./destdir/",
        default=os.path.abspath(os.getcwd()),
    )
    parser.add_argument(
        "--format",
        help="format to be used for the image creation",
        choices=[str(ImageFormat.DDI), str(ImageFormat.COMPRESSED)],
        default=ImageFormat.COMPRESSED,
    )
    parser.add_argument(
        "--private-key",
        help="path to the private key file e.g., files/boot-keys/SYSEXT.key",
        default="",
    )
    parser.add_argument(
        "--certificate",
        help="path to the certificate file, e.g., files/boot-keys/SYSEXT.crt",
        default="",
    )
    parser.add_argument(
        "--ignore-release",
        help="ignore the host release data and use 'ID=_any' instead",
        action="store_true",
    )
    parser.add_argument(
        "--persistent",
        help="add the image under /var/lib/extensions/ to persist after reboots",
        action="store_true",
    )
    parser.add_argument(
        "--system",
        help="build system to use e.g., import",
        choices=[
            str(BuildSystemType.AUTO),
            str(BuildSystemType.BST),
            str(BuildSystemType.MESON),
            str(BuildSystemType.IMPORT),
        ],
        default=BuildSystemType.AUTO,
    )

    args = parser.parse_args()
    _handle_global_args(parser, args)
    _build_args_validator(parser, args)

    try:
        run_image_add_workflow(
            run_image_build_workflow(
                args.name,
                args.source,
                [],
                WORKSPACE_DIR,
                args.format,
                args.private_key,
                args.certificate,
                args.ignore_release,
                args.system,
                args.extension_type,
                False,
            ),
            args.persistent,
            args.extension_type,
        )
    except HandledError as e:
        sys.exit(str(e))
    finally:
        run_cleanup_workflow()
