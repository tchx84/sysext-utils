# MIT License
#
# Copyright (c) 2024 Codethink Limited
# Copyright (c) 2024 GNOME Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# SPDX-License-Identifier: MIT

from typing import List

from .definitions import ExtensionType, WORKSPACE_DIR, INTEGRATION_SYSEXT_NAME
from .helpers import (
    get_active_sysext_names,
    get_sysext_tree,
    get_confext_tree,
    ensure_priviledged,
    remove_directory,
    make_directory,
)
from .utilities import (
    build_image,
    add_image,
    remove_image,
    build_artifact,
    build_tree,
    add_tree,
    remove_tree,
    build_integration_artifact,
)
from .models import Setting


def run_image_build_workflow(
    name: str,
    source: str,
    element: List[str],
    destination: str,
    image_format: str,
    private_key: str,
    certificate: str,
    ignore_release: bool,
    system: str,
    extension_type: ExtensionType,
    force: bool,
) -> List[str]:
    images = []
    artifact, extension_types = build_artifact(source, element, system)

    # Ensure we provide the minimum structure for a valid empty extension
    if force:
        make_directory(get_confext_tree(artifact))
        make_directory(get_sysext_tree(artifact))
        extension_types = [ExtensionType.CONFEXT, ExtensionType.SYSEXT]

    for detected_type in extension_types:
        if extension_type not in [ExtensionType.AUTO, detected_type]:
            continue

        image = build_image(
            name,
            artifact,
            destination,
            image_format,
            private_key,
            certificate,
            ignore_release,
            Setting(detected_type),
        )

        images.append(image)
        print(f"Successfully built {image}")

    return images


def run_image_integration_workflow(setting: Setting) -> None:
    if setting.extension_type != ExtensionType.SYSEXT:
        return

    remove_tree(INTEGRATION_SYSEXT_NAME, setting)
    artifact = build_integration_artifact()
    build_tree(INTEGRATION_SYSEXT_NAME, artifact, False, setting)
    add_tree(INTEGRATION_SYSEXT_NAME, artifact, setting)


def run_image_add_workflow(
    images: List[str],
    persistent: bool,
    extension_type: ExtensionType,
) -> None:
    ensure_priviledged()

    # Validate all models before making any changes to the system
    settings = [(image, Setting.guess(image, persistent)) for image in images]

    for image, setting in settings:
        if extension_type not in [ExtensionType.AUTO, setting.extension_type]:
            continue

        name = setting.get_image_name(image)

        remove_image(name, setting)
        add_image(name, image, setting)
        run_image_integration_workflow(setting)

        print(f"Successfully added {image}")


def run_image_deintegration_workflow(setting: Setting) -> None:
    if setting.extension_type != ExtensionType.SYSEXT:
        return

    remove_tree(INTEGRATION_SYSEXT_NAME, setting)

    if get_active_sysext_names():
        run_image_integration_workflow(setting)


def run_image_remove_workflow(name: str, extension_type: ExtensionType) -> None:
    ensure_priviledged()

    for tentative_type in [ExtensionType.CONFEXT, ExtensionType.SYSEXT]:
        if extension_type not in [ExtensionType.AUTO, tentative_type]:
            continue

        if (setting := Setting.find(name, tentative_type)) is None:
            print(f"Could not find {tentative_type} {name}")
            continue

        remove_image(name, setting)
        run_image_deintegration_workflow(setting)

        print(f"Successfully removed {tentative_type} {name}")


def run_cleanup_workflow() -> None:
    remove_directory(WORKSPACE_DIR)
