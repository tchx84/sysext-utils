# MIT License
#
# Copyright (c) 2024 Codethink Limited
# Copyright (c) 2024 GNOME Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# SPDX-License-Identifier: MIT

import os

from enum import StrEnum
from sysconfig import get_config_var

LIB_DIR = os.path.relpath(get_config_var("LIBDIR"), os.sep)
OS_RELEASE_PATH = os.path.join(os.sep, "etc", "os-release")
RUN_EXTENSIONS_DIR = os.path.join(os.sep, "run", "extensions")
VAR_EXTENSIONS_DIR = os.path.join(os.sep, "var", "lib", "extensions")
RUN_CONFEXTS_DIR = os.path.join(os.sep, "run", "confexts")
VAR_CONFEXTS_DIR = os.path.join(os.sep, "var", "lib", "confexts")
SCHEMAS_DIR = os.path.join("usr", "share", "glib-2.0", "schemas")
ICONS_DIR = os.path.join("usr", "share", "icons", "hicolor")
ICONS_CACHE_FILENAME = "icon-theme.cache"
QUERY_MODULES_DIR = os.path.join(LIB_DIR, "gio", "modules")
QUERY_MODULES_CACHE_FILENAME = "giomodule.cache"
METADATA_DIR = os.path.join("usr", "lib", "extension-release.d")
METADATA_CONFEXT_DIR = os.path.join("etc", "extension-release.d")
METADATA_PREFIX = "extension-release."
METADATA_CUSTOM_FIELD = "SYSEXT_UTILS_VERSION"
SYSEXT_IMAGE_SUFFIX = ".sysext.raw"
CONFEXT_IMAGE_SUFFIX = ".confext.raw"
WORKSPACE_DIR = os.path.join(os.getcwd(), ".sysext-utils")
INTEGRATION_SYSEXT_NAME = "sysext-utils-integration"


class ImageFormat(StrEnum):
    DDI = "ddi"
    COMPRESSED = "compressed"


class DryStepType(StrEnum):
    RUNNING = "running"
    MAKING = "making"
    WRITING = "writing"
    MOVING = "moving"
    REMOVING = "removing"
    COPYING = "copying"
    CHECKING = "checking"


class BuildSystemType(StrEnum):
    AUTO = "auto"
    BST = "bst"
    BST_ELEMENT = "bst-element"
    MESON = "meson"
    IMPORT = "import"
    UNKOWN = "unknown"


class ExtensionType(StrEnum):
    AUTO = "auto"
    SYSEXT = "sysext"
    CONFEXT = "confext"
