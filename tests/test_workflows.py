# MIT License
#
# Copyright (c) 2024 Codethink Limited
# Copyright (c) 2024 GNOME Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# SPDX-License-Identifier: MIT

from sysext_utils.definitions import ImageFormat, BuildSystemType, ExtensionType
from sysext_utils.workflows import (
    run_image_build_workflow,
    run_image_add_workflow,
    run_image_remove_workflow,
)

from .utils import dry


@dry
def test_image_build_workflow():
    run_image_build_workflow(
        name="name",
        source="source",
        element=[],
        destination="destination",
        image_format=ImageFormat.DDI,
        private_key="private_key",
        certificate="certificate",
        ignore_release=False,
        system=BuildSystemType.BST,
        extension_type=ExtensionType.SYSEXT,
        force=False,
    )


@dry
def test_image_build_workflow_with_bst_element():
    run_image_build_workflow(
        name="name",
        source="",
        element=["sdk/gtk.bst"],
        destination="destination",
        image_format=ImageFormat.DDI,
        private_key="private_key",
        certificate="certificate",
        ignore_release=False,
        system=BuildSystemType.BST_ELEMENT,
        extension_type=ExtensionType.SYSEXT,
        force=False,
    )


@dry
def test_image_build_workflow_with_import():
    run_image_build_workflow(
        name="name",
        source="source",
        element=[],
        destination="destination",
        image_format=ImageFormat.DDI,
        private_key="private_key",
        certificate="certificate",
        ignore_release=False,
        system=BuildSystemType.IMPORT,
        extension_type=ExtensionType.SYSEXT,
        force=False,
    )


@dry
def test_image_build_workflow_with_meson():
    run_image_build_workflow(
        name="name",
        source="source",
        element=[],
        destination="destination",
        image_format=ImageFormat.DDI,
        private_key="private_key",
        certificate="certificate",
        ignore_release=False,
        system=BuildSystemType.MESON,
        extension_type=ExtensionType.SYSEXT,
        force=False,
    )


@dry
def test_image_build_workflow_with_compressed_format():
    run_image_build_workflow(
        name="name",
        source="source",
        element=[],
        destination="destination",
        image_format=ImageFormat.COMPRESSED,
        private_key="private_key",
        certificate="certificate",
        ignore_release=False,
        system=BuildSystemType.MESON,
        extension_type=ExtensionType.SYSEXT,
        force=False,
    )


@dry
def test_image_build_work_with_ignore_release():
    run_image_build_workflow(
        name="name",
        source="source",
        element=[],
        destination="destination",
        image_format=ImageFormat.COMPRESSED,
        private_key="private_key",
        certificate="certificate",
        ignore_release=True,
        system=BuildSystemType.MESON,
        extension_type=ExtensionType.SYSEXT,
        force=False,
    )


@dry
def test_image_add_workflow():
    run_image_add_workflow(
        images=[
            "image.sysext.raw",
            "image.confext.raw",
        ],
        persistent=False,
        extension_type=ExtensionType.SYSEXT,
    )


@dry
def test_image_add_workflow_with_remote_image():
    run_image_add_workflow(
        images=[
            "https://image.sysext.raw",
            "https://image.confext.raw",
        ],
        persistent=False,
        extension_type=ExtensionType.SYSEXT,
    )


@dry
def test_image_add_workflow_with_persistent_flag():
    run_image_add_workflow(
        images=[
            "image.sysext.raw",
            "image.confext.raw",
        ],
        persistent=True,
        extension_type=ExtensionType.SYSEXT,
    )


@dry
def test_image_add_workflow_without_integrations():
    run_image_add_workflow(
        images=[
            "image.sysext.raw",
            "image.confext.raw",
        ],
        persistent=False,
        extension_type=ExtensionType.SYSEXT,
    )


@dry
def test_image_remove_workflow():
    run_image_remove_workflow(
        name="name",
        extension_type=ExtensionType.SYSEXT,
    )
