# MIT License
#
# Copyright (c) 2024 Codethink Limited
# Copyright (c) 2024 GNOME Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# SPDX-License-Identifier: MIT

import os

from sysext_utils.definitions import METADATA_DIR, BuildSystemType
from sysext_utils.helpers import get_active_sysext_names, guess_system_from_source


TEST_DATA_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data")


def test_get_active_sysext_names():
    path = os.path.join(TEST_DATA_DIR, "test", METADATA_DIR)
    assert len(get_active_sysext_names(path)) == 1


def test_guess_system_from_source():
    path = os.path.join(TEST_DATA_DIR, "test", "sample", "hierarchy")
    assert guess_system_from_source(path) == BuildSystemType.IMPORT

    path = os.path.join(TEST_DATA_DIR, "test", "sample", "meson")
    assert guess_system_from_source(path) == BuildSystemType.MESON

    path = os.path.join(TEST_DATA_DIR, "test", "sample", "bst")
    assert guess_system_from_source(path) == BuildSystemType.BST
