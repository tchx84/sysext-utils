# MIT License
#
# Copyright (c) 2024 Codethink Limited
# Copyright (c) 2024 GNOME Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# SPDX-License-Identifier: MIT

import pytest


from sysext_utils.errors import (
    CommandNotAvailableError,
    CommandFailedError,
    EmptyDirectoryError,
    UnsupportedImageFormatError,
    UnsupportedBuildSystemError,
    IncompatibleExtensionTypeError,
)
from sysext_utils.helpers import ensure_directory
from sysext_utils.commands import BaseCommand
from sysext_utils.definitions import ExtensionType
from sysext_utils.utilities import build_image, build_artifact
from sysext_utils.models import Setting

from .utils import dry


class NoExistentCommand(BaseCommand):
    @classmethod
    def run(cls, *args) -> None:
        cls.do_run("non-existent-command", *args)


class AlwaysFailsCommand(BaseCommand):
    @classmethod
    def run(cls, *args) -> None:
        cls.do_run("false", *args)


def test_command_not_available_error():
    with pytest.raises(CommandNotAvailableError) as _:
        NoExistentCommand.run([])


def test_command_failed_error():
    with pytest.raises(CommandFailedError) as _:
        AlwaysFailsCommand.run([])


def test_empty_directory_error():
    with pytest.raises(EmptyDirectoryError) as _:
        ensure_directory("does_not_exists")


@dry
def test_unsupported_image_format_error():
    with pytest.raises(UnsupportedImageFormatError) as _:
        build_image(
            name="name",
            artifact="artifact",
            destination="destination",
            image_format="BOGUS",
            private_key=None,
            certificate=None,
            ignore_release=True,
            setting=Setting(ExtensionType.SYSEXT),
        )


@dry
def test_unsupported_build_system_error():
    with pytest.raises(UnsupportedBuildSystemError) as _:
        build_artifact(
            source="source",
            element=[],
            system="BOGUS",
        )


def test_imcompatible_extension_type_error():
    with pytest.raises(IncompatibleExtensionTypeError) as _:
        Setting(extension_type=ExtensionType.SYSEXT).ensure("wrong.confext.raw")
